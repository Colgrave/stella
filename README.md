# Plasma Bigscreen Install Script
A [Plasma Bigscreen](https://plasma-bigscreen.org/) install script for Arch Linux.

To use this install script, it is assumed that you have a functional Arch Linux system logged in as a **regular user** with **sudo privileges**, and have internet connectivity. 

Please note that this install script only provides the essential components for operating Plasma Bigscreen.

This post-install script is intended for people who have experience with Linux, and **I am not liable if you break your operating system**.

## Quick start
```sh
bash <(curl -s https://codeberg.org/Colgrave/pbinstall/raw/branch/main/pbinstall.sh)
```
